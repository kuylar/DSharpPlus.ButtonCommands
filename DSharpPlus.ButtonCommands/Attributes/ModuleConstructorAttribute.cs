using System;

namespace DSharpPlus.ButtonCommands.Attributes
{
    [AttributeUsage(AttributeTargets.Constructor)]
    public sealed class ModuleConstructorAttribute : Attribute
    {
    }
}
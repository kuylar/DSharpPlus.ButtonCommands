using System.Threading.Tasks;
using DSharpPlus.Entities;

namespace DSharpPlus.ButtonCommands.Converters
{
	public interface IButtonArgumentConverter
	{ }
	
	public interface IButtonArgumentConverter<T> : IButtonArgumentConverter
	{
		Task<Optional<T>> ConvertAsync(string value, ButtonContext ctx);
		
		string ConvertToString(T value);
	}
}